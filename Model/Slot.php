<?php

namespace App\Model;

class Slot
{
    private $title;

    private $code;

    public function __construct(string $title, string $code)
    {
        $this->title = $title;
        $this->code = $code;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getCode(): string
    {
        return $this->code;
    }
}