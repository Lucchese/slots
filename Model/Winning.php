<?php

namespace App\Model;

class Winning
{
    private $slots;

    private $prize;

    private $position;

    public function __construct(array $slots, int $prize, string $position)
    {
        $this->position = $position;
        $this->prize = $prize;
        $this->slots = $slots;
    }

    public function getSlots(): array
    {
        return $this->slots;
    }

    public function getPrize(): int
    {
        return $this->prize;
    }

    public function getPosition(): string
    {
        return $this->position;
    }
}