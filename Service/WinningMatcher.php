<?php

namespace App\Service;


use App\Factory\WinningFactory;
use App\Model\Winning;

class WinningMatcher
{

    private $winnerFactory;

    public function __construct(WinningFactory $winnerFactory, $numbersDrum)
    {
        $this->winnerFactory = $winnerFactory;
    }

    public function match(array $slots)
    {
        $winningBySlots = [];
        foreach ($this->winnerFactory->getWinnings() as $winning) {
            if (!array_diff($winning->getSlots(), $slots)) {
                $winningBySlots[] = $winning;
            }
        }

        return $this->getMaxPriceWinner($winningBySlots);
    }

    private function getMaxPriceWinner(array $winnings): ?Winning
    {
        if (count($winnings) === 0) {
            return null;
        }

        usort($winnings, function (Winning $a, Winning $b) {
            if($a->getPrize() === $b->getPrize()) {
                return 0;
            }

            return $a->getPrize() < $b->getPrize() ? 1 : -1;
        });

        return reset($winnings);
    }
}