<?php

namespace App\Service;

use App\Exception\SlotNotFoundException;
use App\Factory\SlotFactory;
use App\Model\Slot;

class SlotService
{
    private $slotFactory;

    public function __construct(SlotFactory $slotFactory)
    {
        $this->slotFactory = $slotFactory;
    }

    /**
     * @param string $code
     * @return Slot
     * @throws SlotNotFoundException
     */
    public function getSlotByCode(string $code): Slot
    {
        $slots = $this->slotFactory->getSlots();
        foreach ($slots as $slot) {
            if ($slot->getCode() === $code) {
                return $slot;
            }
        }

        throw new SlotNotFoundException(sprintf('Slot by code "%s" not found.', $code));
    }

    /**
     * @param array $codes
     * @return array
     * @throws SlotNotFoundException
     */
    public function getSlotsByCodes(array $codes): array
    {
        $slots = [];
        foreach ($codes as $code) {
            $slots[] = $this->getSlotByCode($code);
        }

        return $slots;
    }
}