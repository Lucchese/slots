<?php

namespace App\Service;

use App\Factory\SlotFactory;
use App\Model\Slot;

class RandomSlot
{
    private $slotFactory;

    public function __construct(SlotFactory $slotFactory)
    {
        $this->slotFactory = $slotFactory;
    }

    public function getSlot(): Slot
    {
        $slots = $this->slotFactory->getSlots();

        return $slots[mt_rand(0, count($slots) - 1)];
    }
}