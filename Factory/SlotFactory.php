<?php

namespace App\Factory;

use App\Model\Slot;

class SlotFactory
{

    private $slots = [];

    public function __construct(array $slotsConfig)
    {
        foreach ($slotsConfig as $slotConfig) {
            $this->slots[] = new Slot($slotConfig['title'], $slotConfig['code']);
        }
    }

    /**
     * @return Slot[]
     */
    public function getSlots(): array
    {
        return $this->slots;
    }
}