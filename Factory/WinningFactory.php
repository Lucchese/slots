<?php

namespace App\Factory;

use App\Exception\PositionNotFoundException;
use App\Model\Slot;
use App\Model\Winning;
use App\Service\SlotService;

class WinningFactory
{
    /**
     * @return Winning[]
     */
    private $winnings = [];

    private $slotService;

    /**
     * WinningFactory constructor.
     * @param SlotService $slotService
     * @param array $slotsConfig
     * @throws \App\Exception\SlotNotFoundException
     * @throws PositionNotFoundException
     */
    public function __construct(SlotService $slotService, array $slotsConfig)
    {
        $this->slotService = $slotService;

        $this->initialize($slotsConfig);
    }

    /**
     * @return Winning[]
     */
    public function getWinnings()
    {
        return $this->winnings;
    }

    /**
     * @param array $slotsConfig
     * @throws \App\Exception\SlotNotFoundException
     * @throws PositionNotFoundException
     */
    private function initialize(array $slotsConfig): void
    {
        foreach ($slotsConfig as $slotConfig) {

            if (isset($slotConfig['position']) && !in_array($slotConfig['position'], ['fixed', 'any'])) {
                throw new PositionNotFoundException();
            }

            $this->winnings[] = new Winning(
                $this->slotService->getSlotsByCodes($slotConfig['codes']),
                $slotConfig['prize'],
                isset($slotConfig['position']) ? $slotConfig['position'] : 'any'
            );
        }
    }
}